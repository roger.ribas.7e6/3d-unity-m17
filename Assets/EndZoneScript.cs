using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndZoneScript : MonoBehaviour
{
    private MeshRenderer m_MeshRenderer;
    private void Start()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();

    }
    private void Update()
    {
        m_MeshRenderer.enabled = FindAnyObjectByType<PlayerController>().howMuchKeys >= 3;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (FindAnyObjectByType<PlayerController>().howMuchKeys == 3)
        {
            StartCoroutine(EndPhase());
        }
    }

    private IEnumerator EndPhase()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(3f);
        UnityEngine.Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("End");
    }
}
