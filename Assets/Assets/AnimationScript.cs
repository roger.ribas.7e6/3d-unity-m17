using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    public Animator _anim;
    private PlayerInput pInput;
    private Vector2 _position;
    private Rigidbody _rb;
    public int howMuchKeys;

    public float _speed;
    public float sensibility;
    private Quaternion _initialRot;
    public GameObject cameraCine; //Carlos guapo uwu
    private bool canJump; //booleano que condiciona el cooldown del salto
    

    void Start()
    {
        _initialRot = transform.rotation;
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine(CargarPosicion());
        if(PlayerPrefs.HasKey("keys"))
        {
            howMuchKeys = PlayerPrefs.GetInt("keys");
        }
    }


    private void Awake()
    {
        canJump = true;
        howMuchKeys = 0;
        _rb = GetComponent<Rigidbody>();
        pInput = GetComponent<PlayerInput>();
    }

    
    void Update()
    {
        //Al final el jugador se mueve en direccion donde mira la c�mara debido a que por algun motivo estaba dando muchos errores cinemachine, de hecho esta parte la hicimos juntos ;)
        transform.rotation = Camera.main.transform.rotation;//new Vector3(0, Camera.main.transform.rotation.y, 0);
        Vector3 direction = transform.position - Camera.main.transform.position;
        /* He encontrado una solucion un tanto rara para hacer menos tedioso el movimiento, debido a que daba errores el cinemachine (creo que por la versi�n que usaba) He decidido obligar a tener que pulsar una tecla para   
        el movimiento del personaje, haciendo as� mas sencillo todo */
        if (Input.GetKey(KeyCode.W))
        {
            _rb.velocity = new Vector3(direction.normalized.x, _rb.velocity.y / _speed, direction.normalized.z) * _speed;//transform.TransformDirection(new Vector3(_position.x, _rb.velocity.y / _speed, _position.y)*_speed);
        }
        else
        {
            _rb.velocity = Vector3.Scale(_rb.velocity, Vector3.up);
        }


        _anim.SetFloat("Velocity", _rb.velocity.magnitude);


        //Carlos acuerdate que nos cargamos este input cuando me estuviste mirando los bugs raros de camara y movimiento porque por algun motivo petaba

        // A continuaci�n tenemos el antiguo controlador de movimiento el cual funcionaba sin la camara, pero daba errores de rotacion y de camara al poner el cinemachine ;)

        //_position = pInput.actions["Walk"].ReadValue<Vector2>();
        //transform.rotation = Quaternion.Euler( new Vector3(Camera.main.transform.rotation.x, Camera.main.transform.rotation.y, Camera.main.transform.rotation.z));
        //_rb.velocity = Vector3.Scale(new Vector3(_position.x, _rb.velocity.y / _speed, _position.y), transform.forward) * _speed;
    }



    private void OnEnable()
    {
        
        //Suscripci�n de eventos para el input controller
        Debug.Log(canJump);
        pInput.actions["Jump"].performed += Jump;
        pInput.actions["Reset"].performed += ResetKeys;
        pInput.actions["Run"].performed += Run;
        pInput.actions["Run"].canceled += DisableRun;
        pInput.actions["Emote"].performed += Emote;
        pInput.actions["Crouch"].performed += Crouch;
        
    }
    private void OnDisable()
    {

    }

    private void Jump(InputAction.CallbackContext ctx)
    {
        StartCoroutine(JumpCor(ctx));
    }

    /* El sistema de movimiento basico funciona segun velocidades, un sistema que para un juego t�cnico es funcional y funciona muy bien, aunque para extrapolarlo a un videojuego comercial no es nada recomendable
    el uso de este tipo de condicionadores */
    private void Run(InputAction.CallbackContext ctx)
    {
        _speed *= 2;
    }

    private void DisableRun(InputAction.CallbackContext ctx)
    {
        _speed /= 2f;
    }

    private void Crouch (InputAction.CallbackContext ctx)
    {
        _speed /= 2f;
    }

    private void Emote(InputAction.CallbackContext ctx)
    {
        StartCoroutine(EmoteCor(ctx));
        
    }

    private void ResetKeys(InputAction.CallbackContext ctx)
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("GameScene");
    }

    //Corrutina para hacer el emote
    IEnumerator CargarPosicion()
    {
        yield return new WaitForSeconds(.3f);
        if (PlayerPrefs.HasKey("Pos X"))
        {
            transform.position = new(PlayerPrefs.GetFloat("Pos X"),
            PlayerPrefs.GetFloat("Pos Y"),
            PlayerPrefs.GetFloat("Pos Z"));
            Debug.Log("Tp a: " + new Vector3(PlayerPrefs.GetFloat("Pos X"),
            PlayerPrefs.GetFloat("Pos Y"),
            PlayerPrefs.GetFloat("Pos Z")));
        };
    }

    IEnumerator EmoteCor(InputAction.CallbackContext ctx)
    {
        _anim.SetTrigger("Emote 1");
        _speed *= 0;
        yield return new WaitForSeconds(2f);
        _speed = 5;
    }

    IEnumerator JumpCor(InputAction.CallbackContext ctx)
    {
        if (canJump)
        {
            canJump = false;
            _rb.velocity = new Vector3(_position.x, 1, _position.y) * _speed;
            _anim.SetBool("CanJump", false);
            _anim.SetTrigger("Jump 0");
            _anim.SetBool("CanJump", true);
            yield return new WaitForSeconds(3f);
            canJump = true;
        }
    }
}
