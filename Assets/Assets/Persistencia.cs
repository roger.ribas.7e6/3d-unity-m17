using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persistencia : MonoBehaviour
{
    // Start is called before the first frame update


    // Update is called once per frame
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("A");
        if(collision.gameObject.TryGetComponent(out PlayerController pc))
        {
            PlayerPrefs.SetFloat("Pos X", pc.transform.position.x);
            PlayerPrefs.SetFloat("Pos Y", pc.transform.position.y);
            PlayerPrefs.SetFloat("Pos Z", pc.transform.position.z);
            PlayerPrefs.SetInt("keys", pc.howMuchKeys);
        }
    }


}
