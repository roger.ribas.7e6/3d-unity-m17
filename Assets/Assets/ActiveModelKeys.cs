using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActiveModelKeys : MonoBehaviour
{
    public void ActiveKeys()
    {
        Transform[] a = GetComponentsInChildren<Transform>(true).Where(item => !item.gameObject.activeSelf).ToArray();
        if (a.Length > 0)
        {
            Debug.Log("Llave modelo");
            a[0].gameObject.SetActive(true);
        }
    }
}
