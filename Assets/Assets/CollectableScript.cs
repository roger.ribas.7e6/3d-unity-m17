using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour
{
    private void Start()
    {
        if (PlayerPrefs.HasKey(name) && PlayerPrefs.GetInt(name) == 1)
        {
            FindAnyObjectByType<UIscript>().GetKey();
            FindAnyObjectByType<ActiveModelKeys>().ActiveKeys();
            Destroy(gameObject);
        }
    }
    //Script sencillo ontrigger para activar la parte visual de la UI
    private void OnTriggerEnter(Collider other)
    {
        GetComponent<AudioSource>().Play();
        FindAnyObjectByType<UIscript>().GetKey();
        FindAnyObjectByType<ActiveModelKeys>().ActiveKeys();
        StartCoroutine(DestroyKey());
        FindAnyObjectByType<PlayerController>().howMuchKeys++;
        PlayerPrefs.SetInt(name, 1);
        PlayerPrefs.SetInt("Keys", PlayerPrefs.GetInt("Keys")+1);
    }
    IEnumerator DestroyKey()
    {
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }
}
