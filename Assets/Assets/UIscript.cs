using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UIscript : MonoBehaviour
{
    //Funcion en la que agrupamos todas las llaves de la UI para indicar que el jugador tiene X numero de llaves conseguidas
    public void GetKey()
    {
        //recogemos las llaves que no estan activas mediante el GetComponentsInChildren(true) el booleano true actua de manera que nos permite recogerlas a�n estando inactivas
        Transform[] a = GetComponentsInChildren<Transform>(true).Where(item => !item.gameObject.activeSelf).ToArray();

        //Vamos activandolas seg�n las que hayamos recogido, no necesitamos de un orden ni dependemos de un numero determinado de llaves, por lo que es escalable y reutilizable para otros componentes

        if (a.Length > 0)
        {
            a[0].gameObject.SetActive(true);
        }
    }
}
